//
//  HomeTableViewCell.swift
//  TwitchApp
//
//  Created by Marcos Barbosa on 22/04/2018.
//  Copyright © 2018 Marcos Barbosa. All rights reserved.
//

import UIKit

class HomeTableViewCell: UITableViewCell {

    @IBOutlet weak var gameAnotherImage: UIImageView!
    @IBOutlet weak var gameView: UIView!
    @IBOutlet weak var gameImage: UIImageView!
    @IBOutlet weak var gameName: UILabel!
    @IBOutlet weak var gamePopularity: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
