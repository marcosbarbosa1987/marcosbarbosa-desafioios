//
//  Images.swift
//  TwitchApp
//
//  Created by Marcos Barbosa on 22/04/2018.
//  Copyright © 2018 Marcos Barbosa. All rights reserved.
//

import Foundation

class  Images {
    
    private var large:      String
    private var medium:     String
    private var small:      String
    private var template:   String
    
    init(large: String, medium: String, small: String, template: String) {
        
        self.large =    large
        self.medium =   medium
        self.small =    small
        self.template = template
        
    }
    
    init() {
        self.large = ""
        self.medium = ""
        self.small = ""
        self.template = ""
    }
    
//    large
    public func getLarge() -> String{
        return self.large
    }
    
    public func setLarge(large: String){
        self.large = large
    }
    
//    medium
    public func getMedium() -> String{
        return self.medium
    }
    
    public func setMedium(medium: String){
        self.medium = medium
    }
    
//    small
    public func getSmall() -> String{
        return self.small
    }
    
    public func setSmall(small: String){
        self.small = small
    }
    
//    template
    public func getTemplate() -> String{
        return self.template
    }
    
    public func setTemplate(template: String){
        self.template = template
    }
}
