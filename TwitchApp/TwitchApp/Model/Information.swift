//
//  Information.swift
//  TwitchApp
//
//  Created by Marcos Barbosa on 22/04/2018.
//  Copyright © 2018 Marcos Barbosa. All rights reserved.
//

import Foundation

class Information{
    
    private var channels:   Int
    private var viewers:    Int
    private var game:       Game
    
    init(channel: Int, view: Int, game: Game) {
        
        self.channels = channel
        self.viewers  = view
        self.game     = game
        
    }
    
    init() {
        self.channels   = 0
        self.viewers    = 0
        self.game       = Game()
    }
    
//    channels
    public func getChannels() -> Int{
        return self.channels
    }
    
    public func setChannels(channel: Int){
        self.channels = channel
    }
    
//    viewers
    public func getViewers() -> Int{
        return self.viewers
    }
    
    public func setViewers(view: Int){
        self.viewers = view
    }
    
//    game
    public func getGame() -> Game{
        return self.game
    }
    
    public func setGame(game: Game){
        self.game = game
    }
}
