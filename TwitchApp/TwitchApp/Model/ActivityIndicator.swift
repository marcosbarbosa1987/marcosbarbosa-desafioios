//
//  ActivityIndicator.swift
//  NavilleLinhaDireta
//
//  Created by Marcos Barbosa on 23/01/2018.
//  Copyright © 2018 Marcos Barbosa. All rights reserved.
//

import Foundation
import UIKit
import NVActivityIndicatorView

class ActivityIndicator {
    
    var activityData: ActivityData!
    
    func startActivity() {
        
        let size = CGSize(width: 40, height: 40)
        let font = UIFont(name: "Avenir Next", size: 17)
        
        activityData = ActivityData(size: size, message: "Carregando...", messageFont: font, type: .lineSpinFadeLoader, color: UIColor(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), padding: nil, displayTimeThreshold: nil, minimumDisplayTime: nil, backgroundColor:  UIColor(red: 0, green: 0, blue: 0, alpha: 0.5), textColor: nil)
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData)
        
    }
    
    func stopActivity() {
        NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
    }
    
}
