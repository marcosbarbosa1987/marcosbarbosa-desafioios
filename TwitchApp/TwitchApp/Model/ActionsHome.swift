//
//  ActionsHome.swift
//  TwitchApp
//
//  Created by Marcos Barbosa on 21/04/2018.
//  Copyright © 2018 Marcos Barbosa. All rights reserved.
//

import Foundation

extension HomeViewController: retornoJsonDelegate{
    func json(json: Dictionary<String, AnyObject>) {
        
        print(json)
        
        if let response = json["top"] as? NSArray{
            
            //self.dataTwitch = [Information]()
            
            if self.functionController == 1 {
                if response.count > 0 {
                    self.dataTwitch = [Information]()
                }
            }
            
            for i in response{
                
                if let eachInformation = i as? NSDictionary{
                    
                    let unitData = Information()
                    
                    if let channels = (eachInformation["channels"] as AnyObject).integerValue{
                        unitData.setChannels(channel: channels)
                        
                    }
                    
                    if let viewers = (eachInformation["viewers"] as AnyObject).integerValue{
                        unitData.setViewers(view: viewers)
                        
                    }
                    
                    if let game = eachInformation["game"] as? NSDictionary{
                        
                        let unitGame = Game()
                        
                        if let id = (game["_id"] as AnyObject).integerValue{
                            unitGame.setID(id: id)
                        }
                        
                        if let links = game["_links"] as? String{
                            //MARK : TODO: i don´t know yet
                        }
                        
                        if let giantBomb = (game["giantbomb_id"] as AnyObject).integerValue{
                            unitGame.setGiantBombID(giant: giantBomb)
                        }
                        
                        if let locale = game["locale"] as? String{
                            unitGame.setLocale(locale: locale)
                        }
                        
                        if let localizedName = game["localized_name"] as? String{
                            unitGame.setLocalizedName(name: localizedName)
                        }
                        
                        if let name = game["name"] as? String{
                            unitGame.setName(name: name)
                            
                        }
                        
                        if let popularity = (game["popularity"] as AnyObject).integerValue{
                            unitGame.setPopularity(popularity: popularity)
                        }
                        
                        if let box = game["box"] as? NSDictionary{
                            
                            let boxImages = Images()
                            
                            if let large = box["large"] as? String{
                                boxImages.setLarge(large: large)
                            }
                            
                            if let medium = box["medium"] as? String{
                                boxImages.setMedium(medium: medium)
                            }
                            
                            if let small = box["small"] as? String{
                                boxImages.setSmall(small: small)
                            }
                            
                            if let template = box["template"] as? String{
                                boxImages.setTemplate(template: template)
                            }
                            
                            unitGame.setBox(box: boxImages)
                        }
                        
                        if let logo = game["logo"] as? NSDictionary{
                            
                            let logoImages = Images()
                            
                            if let large = logo["large"] as? String{
                                logoImages.setLarge(large: large)
                            }
                            
                            if let medium = logo["medium"] as? String{
                                logoImages.setMedium(medium: medium)
                            }
                            
                            if let small = logo["small"] as? String{
                                logoImages.setSmall(small: small)
                            }
                            
                            if let template = logo["template"] as? String{
                                logoImages.setTemplate(template: template)
                            }
                            
                            unitGame.setLogo(logos: logoImages)
                        }
                        unitData.setGame(game: unitGame)
                    }
                    
                    if self.dataTwitch.contains(where: {$0.getGame().getID() == unitData.getGame().getID()}){
                        print("dado ja existe no array")
                    }else{
                        self.dataTwitch.append(unitData)
                    }
                    
                }
                
            }
            
            refreshing.endRefreshing()
            self.tableTwitch.reloadData()
        }
        
        if let links = json["_links"] as? NSDictionary{
        
            self.pages = Pages()
            if let next = links["next"] as? String{
                self.pages.setNext(next: next)
                self.booleanPage = false 
            }
            
            if let actually = links["actually"] as? String{
                self.pages.setActually(actually: actually)
            }
            
        }
        
    }
    
    internal func requestData(){
        
        self.functionController = 1
        let ws = WebService(
            funcao: self.shared.client_id,
            campos:"",
            authUsuario:"",
            authSenha:"",
            tipo:"GET",
            ativarIndicator: false,
            token: ""
        );
        
        ws.setUrlServidor(urlServidor: self.shared.server)
        ws.conectar()
        ws.retornoDelegate = self
        
    }
    
    internal func updateData(){
        
        self.functionController = 2
        let ws = WebService(
            funcao: "",
            campos:"",
            authUsuario:"",
            authSenha:"",
            tipo:"GET",
            ativarIndicator: true,
            token: ""
        );
        
        ws.setUrlServidor(urlServidor: self.pages.getNext() + "&" + self.shared.client_id)
        ws.conectar()
        ws.retornoDelegate = self
        
    }
    
}
