//
//  WebService.swift
//  NavilleLinhaDireta
//
//  Created by Marcos Barbosa on 23/01/2018.
//  Copyright © 2018 Marcos Barbosa. All rights reserved.
//

import Foundation

@objc protocol retornoJsonDelegate {
    func json(json: Dictionary<String, AnyObject>)
    @objc optional func jsonArray(json: NSArray)
}

class WebService: retornoJsonDelegate{
    
    internal func json(json: Dictionary<String, AnyObject>) {}
    
    
    public var retornoDelegate: retornoJsonDelegate? = nil
    
    //variavel do loading
    let indicator = ActivityIndicator()
    
    private var urlServidor : String = ""
    private var funcao : String         /*Caso seja GET enviar os parametros concatenados
     (FUNCAO?CAMPO=VALOR&CAMPO=VALOR&...)*/
    
    private var campos : String         /*Campo(s) para enviar (CAMPO=VALOR&CAMPO=VALOR&...)
     OPCIONAL, ENVIAR ""*/
    private var authUsuario : String    //Usuário para autenticação OPCIONAL, ENVIAR ""/
    private var authSenha : String      //Usuário para autenticação OPCIONAL, ENVIAR ""/
    private var tipo : String           //DELETE,GET,POST ou PUT/
    private var ativarIndicator: Bool
    private var token: String
    
    //Instancia com valores.
    init(funcao:String,campos:String,authUsuario:String,authSenha:String,tipo:String,ativarIndicator:Bool, token: String) {
        self.funcao = funcao
        self.campos = campos
        self.authSenha = authSenha
        self.authUsuario = authUsuario
        self.tipo = tipo
        self.ativarIndicator = ativarIndicator
        self.token = token
    }
    
    //Instancia Em Branco.
    init() {
        self.funcao = ""
        self.campos = ""
        self.authSenha = ""
        self.authUsuario = ""
        self.tipo = ""
        self.ativarIndicator = false
        self.token = ""
    }
    
    /*
     * Gets e Sets
     * */
    
    public func setUrlServidor(urlServidor:String) {
        self.urlServidor = urlServidor
    }
    
    public func getUrlServidor() -> String {
        return urlServidor
    }
    
    public func setFuncao(funcao:String) {
        self.funcao = funcao
    }
    
    public func getFuncao() -> String {
        return funcao
    }
    
    public func setCampos(campos:String) {
        self.campos = campos
    }
    
    public func getCampos() -> String {
        return campos
    }
    
    public func setAuthUsuario(authUsuario:String) {
        self.authUsuario = authUsuario
    }
    
    public func getAuthUsuario() -> String {
        return authUsuario
    }
    
    public func setAuthSenha(authSenha:String) {
        self.authSenha = authSenha
    }
    
    public func getAuthSenha() -> String {
        return authSenha
    }
    
    public func setTipo(tipo:String) {
        self.tipo = tipo
    }
    
    public func getTipo() -> String {
        return tipo
    }
    
    public func getAtivarIndicator() -> Bool{
        return ativarIndicator
    }
    
    public func setAtivarIndicator(ativarIndicator: Bool){
        self.ativarIndicator = ativarIndicator
    }
    
    public func getToken() -> String{
        return self.token
    }
    
    public func setToken(token: String){
        self.token = token
    }
    
    /*
     *  Funções específicas do WebService.
     * */
    
    public func exibirDados(){
        
        print("""
            Campos preenchidos:
            URL: \(getUrlServidor())
            Função: \(getFuncao())
            Tipo de requisição: \(getTipo())
            Usuário: \(getAuthUsuario())
            Senha: \(getAuthSenha())
            Parametros: \(getCampos())
            """);
        
    }
    
    public func conectar(){
        //Printando antes de enviar.
        self.exibirDados()
        
        if getAtivarIndicator(){
            self.indicator.startActivity()
        }
        
        
        
        //variavel de requisicao ira receber o caminho da constante acima
        guard let url = URL(string: self.getUrlServidor()+self.getFuncao()) else {return}
        print("url =\(url)")
        
        var requisicao = URLRequest(url:url)
        
        //informa o tipo de envio
        requisicao.httpMethod = self.getTipo()
        
        //Caso tenha preenchido a autenticação
        if(self.getAuthSenha() != "" && self.getAuthUsuario() != ""){
            //Usar da sessão.
            let usuario = getAuthUsuario()
            let senha   = getAuthSenha()
            //Converte os dados para enviar via HEADER
            let login = String(format: "%@:%@", usuario, senha)
            let loginData = login.data(using: String.Encoding.utf8)!
            let base64LoginString = loginData.base64EncodedString()
            requisicao.setValue("Basic \(base64LoginString)", forHTTPHeaderField: "Authorization")
        }else if (self.getToken() != ""){
            requisicao.addValue("Bearer \(self.getToken())", forHTTPHeaderField: "Authorization")
        }
        
        //Caso não seja GET, e tenha campos
        if(self.getTipo() != "GET" && self.getCampos() != "") {
            //constante ira cricar uma string com o id que o webService espera receber e com valor ja associado
            let camposPost = self.getCampos()
            //abaixo será feira uma associacao de tipo de codificação de caracteres
            requisicao.httpBody = camposPost.data(using: String.Encoding.utf8)
        }
        
        let config: URLSessionConfiguration = .default
        config.httpMaximumConnectionsPerHost = 3
        config.timeoutIntervalForRequest = 20
        let session: URLSession = URLSession(configuration: config)
        
        let task: URLSessionTask = session.dataTask(with: requisicao) { (respostaWs, responseOrNil, errorOrNil)->() in
            DispatchQueue.main.async() {
                
                var errorDescription: String = ""
                
                if let error = errorOrNil {
                    switch error {
                    case URLError.networkConnectionLost, URLError.notConnectedToInternet:
                        print("no network connection")
                        errorDescription = "Sem conexão com a internet!"
                    case URLError.cannotFindHost, URLError.notConnectedToInternet:
                        print("cannot find the host, could be too busy, try again in a little while")
                        errorDescription = "Não foi possível conectar ao servidor!"
                    case URLError.cancelled:
                        print("don´t bother the user, we are doing what they want")
                        errorDescription = "O login foi cancelado!"
                    case URLError.timedOut:
                        print("caiu aqui no timeout")
                        errorDescription = "Parece que a conexão esta meio lenta, tente de novo em instantes!"
                    default:
                        print("unknown error")
                        errorDescription = "Ocorreu um erro, tente de novo em instantes!"
                    }
                    
                    if self.getAtivarIndicator(){
                        self.indicator.stopActivity()
                    }
                    let errorDictionary: Dictionary = ["status": -10, "resultado": "\(errorDescription)"] as [String : AnyObject]
                    
                    if(self.retornoDelegate != nil){
                        self.retornoDelegate?.json(json: errorDictionary as Dictionary<String, AnyObject>)
                    }
                    return
                }
                
                
                if let respostaWs = respostaWs {
                  
                    do{
                        
                        if let json = try JSONSerialization.jsonObject(with: respostaWs, options: JSONSerialization.ReadingOptions.mutableContainers) as? Dictionary<String, AnyObject>{
                        
                            //Retornando para o protocolo o jSON
                            if(self.retornoDelegate != nil){
                                self.retornoDelegate?.json(json: json)
                            }
                            
                            if self.getAtivarIndicator(){
                                self.indicator.stopActivity()
                            }
                        }
                        
                        if let jsonArray = try JSONSerialization.jsonObject(with: respostaWs, options: JSONSerialization.ReadingOptions.mutableContainers) as? NSArray{
                            
                            //Retornando para o protocolo o jSON
                            if(self.retornoDelegate != nil){
                                self.retornoDelegate?.jsonArray!(json: jsonArray)
                            }
                            
                            if self.getAtivarIndicator(){
                                self.indicator.stopActivity()
                            }
                        }
                        
                        
                    }catch {
                        //se nao conseguir se conectar ao webservice ira mostrar o erro aqui
                        print("Erro ao usar o Webservice: \(String(describing: errorOrNil))")
                        
                        if(self.retornoDelegate != nil){
                            let errorDescription = "Ocorreu um erro!"
                            let errorDictionary: Dictionary = ["status": -9, "resultado": "\(errorDescription)"] as [String : AnyObject]
                            self.retornoDelegate?.json(json: errorDictionary as Dictionary<String, AnyObject>)
                        }
                        
                        if self.getAtivarIndicator(){
                            self.indicator.stopActivity()
                        }
                        
                    }
                    
                }
                
                //print("http status = \(httpResponse.statusCode)")
                print("completed")
                
            }
        }
        //group.enter()
        task.resume()
        
        
        
    }
    
}



