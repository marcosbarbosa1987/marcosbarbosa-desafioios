//
//  Game.swift
//  TwitchApp
//
//  Created by Marcos Barbosa on 22/04/2018.
//  Copyright © 2018 Marcos Barbosa. All rights reserved.
//

import Foundation

class Game{
    
    private var id:             Int
    private var links:          [String]
    private var box:            Images
    private var giantBombID:    Int
    private var locale:         String
    private var localizedName:  String
    private var logos:          Images
    private var name:           String
    private var popularity:     Int
    
    
    init(id: Int, giant: Int, popularity: Int, locale: String, localizedName: String, name: String, links: [String], box: Images, logos: Images) {
        
        self.id                 = id
        self.giantBombID        = giant
        self.popularity         = popularity
        self.locale             = locale
        self.localizedName      = localizedName
        self.name               = name
        self.links              = links
        self.box                = box
        self.logos              = logos
        
    }
    
    init() {
        self.id                 = 0
        self.giantBombID        = 0
        self.popularity         = 0
        self.locale             = ""
        self.localizedName      = ""
        self.name               = ""
        self.links              = [String]()
        self.box                = Images()
        self.logos              = Images()
    }
    
//    id
    public func getID() -> Int{
        return self.id
    }
    
    public func setID(id: Int){
        self.id = id
    }
    
//    giantBombID
    public func getGiantBombID() -> Int{
        return self.giantBombID
    }
    
    public func setGiantBombID(giant: Int){
        self.giantBombID = giant
        
    }
    
//    popularity
    public func getPopularity() -> Int{
        return self.popularity
    }
    
    public func setPopularity(popularity: Int){
        self.popularity = popularity
    }
    
//    locale
    public func getLocale() -> String{
        return self.locale
    }
    
    public func setLocale(locale: String){
        self.locale = locale
    }
    
//    localizedName
    public func getLocalizedName() -> String{
        return self.localizedName
    }
    
    public func setLocalizedName(name: String){
        self.localizedName = name
    }
    
//    name
    public func getName() -> String{
        return self.name
    }
    
    public func setName(name: String){
        self.name = name
    }
    
//    links
    public func getLinks() -> [String]{
        return self.links
    }
    
    public func setLinks(links: [String]){
        self.links = links
    }
    
//    box
    public func getBox() -> Images{
        return self.box
    }
    
    public func setBox(box: Images){
        self.box = box
    }
    
//    logos
    public func getLogos() -> Images{
        return self.logos
    }
    
    public func setLogo(logos: Images){
        self.logos = logos
    }
}
