//
//  Pages.swift
//  TwitchApp
//
//  Created by Marcos Barbosa on 22/04/2018.
//  Copyright © 2018 Marcos Barbosa. All rights reserved.
//

import Foundation

class Pages{
    
    private var next: String
    private var actually: String
    
    init(next: String, actually: String) {
        
        self.actually = actually
        self.next     = next
        
    }
    
    init() {
        
        self.actually = ""
        self.next     = ""
        
    }
    
//    next
    public func getNext() -> String{
        return self.next
    }
    
    public func setNext(next: String){
        self.next = next
    }
    
//    actually
    public func getActually() -> String{
        return self.actually
    }
    
    public func setActually(actually: String){
        self.actually = actually
    }
}
