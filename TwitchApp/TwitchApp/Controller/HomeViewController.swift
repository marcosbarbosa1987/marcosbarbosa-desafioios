//
//  HomeViewController.swift
//  TwitchApp
//
//  Created by Marcos Barbosa on 21/04/2018.
//  Copyright © 2018 Marcos Barbosa. All rights reserved.
//

import UIKit
import SDWebImage

class HomeViewController: UIViewController {

    //Elements of screen
    @IBOutlet weak var tableTwitch: UITableView!
    @IBOutlet weak var labelNumberGames: UILabel!
    
    
    
    //variables and constants
    var shared = UIApplication.shared.delegate as! AppDelegate
    var dataTwitch = [Information]()
    var pages      = Pages()
    var booleanPage = false
    var functionController = 0
    var refreshing: UIRefreshControl!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        //configuring table
        self.tableTwitch.delegate = self
        self.tableTwitch.dataSource = self
        self.tableTwitch.tableFooterView = UIView()
        
        requestData()
        
        
        refreshing = UIRefreshControl()
        refreshing.attributedTitle = NSAttributedString(string: "Atualizar")
    refreshing.layer.backgroundColor = #colorLiteral(red: 0.2943529487, green: 0.2281232476, blue: 0.4844736457, alpha: 1)
        refreshing.tintColor = UIColor.white
        refreshing.addTarget(self, action: #selector(didPullToRefresh), for: .valueChanged)
        tableTwitch.addSubview(refreshing)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle{
        return UIStatusBarStyle.lightContent
    }
    
    @objc func didPullToRefresh() {
        requestData()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        let destination = segue.destination as! DetailGameViewController
        destination.game = dataTwitch[sender as! Int]
        
    }
}

extension HomeViewController: UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if self.dataTwitch.count < 1 {
            
            let label = UILabel()
            label.text = "Nenhum dado foi encontrado."
            label.textAlignment = .center
            self.tableTwitch.backgroundView = label
            
        }else{
            self.tableTwitch.backgroundView = nil
        }
        
        self.labelNumberGames.text = "Numero de jogos: \(self.dataTwitch.count)"
        return self.dataTwitch.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = self.tableTwitch.dequeueReusableCell(withIdentifier: "cell") as! HomeTableViewCell
        
        cell.gameImage.layer.shadowColor = UIColor.black.cgColor
        cell.gameImage.layer.shadowOffset = CGSize(width: 3, height: 3)
        cell.gameImage.layer.shadowOpacity = 1.0
        cell.gameImage.layer.shadowRadius = 5
       
        
        cell.gameView.layer.borderColor  = #colorLiteral(red: 0.2943529487, green: 0.2281232476, blue: 0.4844736457, alpha: 1)
        cell.gameView.layer.borderWidth  = 0.8
        cell.gameView.layer.shadowColor  = UIColor.black.cgColor
        cell.gameView.layer.shadowOffset = CGSize(width: 4, height: 3)
        cell.gameView.layer.shadowOpacity = 1.0
        cell.gameView.layer.shadowRadius  = 3
        
        
        
        if let customerImage: String = dataTwitch[indexPath.row].getGame().getBox().getLarge(){
            let url: NSURL? = NSURL(string: customerImage)
            if let urlImage = url{
                cell.gameAnotherImage.sd_setImage(with: urlImage as URL!)
                cell.gameImage.sd_setImage(with: urlImage as URL!)
            }
        }
        
        cell.gameName.text = dataTwitch[indexPath.row].getGame().getName()
        cell.gamePopularity.text = String( describing: dataTwitch[indexPath.row].getGame().getPopularity())
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self.performSegue(withIdentifier: "detailGame", sender: indexPath.row)
        
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if indexPath.row == dataTwitch.count - 1{
            updateData()
        }
        
    }
    
//    func scrollViewDidScroll(_ scrollView: UIScrollView) {
//
//        let offSetY = scrollView.contentOffset.y
//        let contentHeight = scrollView.contentSize.height
//
//        if offSetY > contentHeight - scrollView.frame.size.height{
//
//            if self.booleanPage == false{
//                self.updateData()
//                self.booleanPage = true
//            }
//
//        }
//
//    }
    
}
