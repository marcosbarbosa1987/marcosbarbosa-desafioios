//
//  DetailGameViewController.swift
//  TwitchApp
//
//  Created by Marcos Barbosa on 22/04/2018.
//  Copyright © 2018 Marcos Barbosa. All rights reserved.
//

import UIKit
import SDWebImage

class DetailGameViewController: UIViewController {

    //Elements of screen
    @IBOutlet weak var anotherImageGame: UIImageView!
    @IBOutlet weak var gameImage: UIImageView!
    @IBOutlet weak var gameName: UILabel!
    @IBOutlet weak var gameView: UILabel!
    
    //variables and constants
    var game = Information()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.gameImage.layer.shadowColor = UIColor.black.cgColor
        self.gameImage.layer.shadowOffset = CGSize(width: 4, height: 4)
        self.gameImage.layer.shadowOpacity = 1.0
        self.gameImage.layer.shadowRadius = 4
        
        if let customerImage: String = game.getGame().getBox().getLarge(){
            let url: NSURL? = NSURL(string: customerImage)
            if let urlImage = url{
                anotherImageGame.sd_setImage(with: urlImage as URL!)
                gameImage.sd_setImage(with: urlImage as URL!)
            }
        }
        gameName.text = game.getGame().getName()
        gameView.text = String(describing: game.getViewers())
        
    }

    

}
